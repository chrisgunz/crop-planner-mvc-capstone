﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

console.log(myTestData.canvasPlantInfo)
console.log(myTestData)

function changePlantInfo() {
    if (document.getElementById("exposureList").value == "Full Shade") {
        (Model.RecommendedPlants = Model.FullShadePlants);
    };
};

let canvas = new fabric.Canvas('drawarea', { backgroundColor: "#DEB887" });
document.querySelector('.lower-canvas').classList.add('no-background');


for (let i = 0; i < myTestData.canvasPlantInfo.length; i++) {
    let plot = myTestData.canvasPlantInfo[i];
    let allPlants = myTestData.listAllPlants;
    for (let j = 0; j < allPlants.length; j++) {
        let plant = allPlants[j];
        if (plot.plantId === plant.id) {

            let text = new fabric.Text('', {
                fontSize: 20, color: 'white',
                left: plot.left, top: plot.top,
                
            })

            let rect = new fabric.Rect({
                fill: plant.color, height: (plant.spread / 12) * 50, width: (plant.spread / 12) * 50,
                left: plot.left, top: plot.top,
                borderColor: 'black',
                cornerColor: 'black',
                cornerSize: 5,
                rx: 10,
                ry: 10,
                hasBorders: true,
                hasControls: false,
                transparentCorners: false,
                stroke: 'white',
                strokeWidth: 2,
                cornerSize: 0,
            });
            rect.plantid = plot.plantId;
            let group = new fabric.Group([rect, text], {
                    height: (plant.spread / 12) * 50, width: (plant.spread / 12) * 50,
                    borderColor: 'black',
                    cornerColor: 'black',
                    hasBorders: true,
                    hasControls: false,
                cornerSize: 0,
                left: plot.left, top: plot.top
            });


            canvas.add(group);
            
        }
    }
}


function onAddPlot(num, uniqueId, spread, color) {
    let text = new fabric.Text(num, {
        fontSize: 20,
    });
    text.setColor('white')
    let rect = new fabric.Rect({
        fill: color, height: (spread / 12) * 50, width: (spread / 12) * 50,
        borderColor: 'black',
        cornerColor: 'black',
        cornerSize: 5,
        rx: 10,
        ry: 10,
        hasBorders: true,
        hasControls: false,
        transparentCorners: false,
        stroke: 'white',
        strokeWidth: 2
        

    });
    rect.plantid = uniqueId;
    console.log("rect", rect);
    let group = new fabric.Group([rect, text], {
        height: (spread / 12) * 50, width: (spread / 12) * 50,
        borderColor: 'black',
        cornerColor: 'black',
        hasBorders: true,
        hasControls: false,
        cornerSize: 0,
    });
    canvas.add(group);
}

$("#delete").click(function () {
    deleteObjects();
})

function deleteObjects() {
    var activeObject = canvas.getActiveObject();
    if (activeObject) {
        if (confirm('Are you sure?')) {
            canvas.remove(activeObject);
        }
    }
}

function saveCanvas(canvas, model) {
    let array = canvas._objects.map((o) => { return { top: o.top, left: o.left, plantId: o._objects[0].plantid } });
    console.log(array);
    console.log(JSON.stringify(array));

    let url = "https://localhost:44392/api/plotapi";
    fetch(url, {
        method: "PUT",
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify(array)
    }
    ).then((resp) => {
        console.log('FETCH Completed', resp);
    }
    );
}

//Function should remove hide the currently displayed list by removing the "show-recommendations" class and adding the "hide-recommendations" class
//Funtion should then find the div id that matches the value in the dropdown list of exposures and remove the hidden class from that element and add
//the show class to display the proper list. 
function changeRecommendedList() {
    console.log("Change Recommended List is being called");
    let mainRecommendation = document.getElementById("recommendations");
    let recommendationArray = Array.from(document.getElementsByClassName("secondary-recommendations"));
    recommendationArray.push(mainRecommendation);
    for (let i = 0; i < recommendationArray.length; i++) {
        if (recommendationArray[i].classList.contains("show-recommendations")) {
            recommendationArray[i].classList.remove("show-recommendations");
            recommendationArray[i].classList.add("hide-recommendations");
        }
        else if (recommendationArray[i].id == document.getElementById("exposureList").value) {
            recommendationArray[i].classList.remove("hide-recommendations");
            recommendationArray[i].classList.add("show-recommendations");
        }
    }
}

canvas.on('object:moving', function (e) {
    var obj = e.target;
    // if object is too big ignore
    if (obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width) {
        return;
    }
    obj.setCoords();
    // top-left corner
    if (obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0) {
        obj.top = Math.max(obj.top, obj.top - obj.getBoundingRect().top);
        obj.left = Math.max(obj.left, obj.left - obj.getBoundingRect().left);
    }
    // bot-right corner
    if (obj.getBoundingRect().top + obj.getBoundingRect().height > obj.canvas.height || obj.getBoundingRect().left + obj.getBoundingRect().width > obj.canvas.width) {
        obj.top = Math.min(obj.top, obj.canvas.height - obj.getBoundingRect().height + obj.top - obj.getBoundingRect().top);
        obj.left = Math.min(obj.left, obj.canvas.width - obj.getBoundingRect().width + obj.left - obj.getBoundingRect().left);
    }
});