﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Web.DAL;
using WebApplication.Web.Models;
using WebApplication.Web.Models.Account;
using WebApplication.Web.Providers.Auth;

namespace WebApplication.Web.Controllers
{    
    public class AccountController : Controller
    {
        private readonly IAuthProvider authProvider;
        private IPlotDAO plotDAO;
        private IPlantDAO plantDAO;

        public AccountController(IAuthProvider authProvider, IPlotDAO plotDAO, IPlantDAO plantDAO)
        {
            this.authProvider = authProvider;
            this.plotDAO = plotDAO;
            this.plantDAO = plantDAO;
        }

        //[AuthorizationFilter] // actions can be filtered to only those that are logged in
        [AuthorizationFilter("Admin", "Author", "Manager", "User")]  //<-- or filtered to only those that have a certain role
        [HttpGet]
        public IActionResult Index()
        {
            var user = authProvider.GetCurrentUser();
            return View(user);
        }

        [HttpGet]
        public IActionResult Login()
        {            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginViewModel loginViewModel)
        {
            // Ensure the fields were filled out
            if (ModelState.IsValid)
            {
                // Check that they provided correct credentials
                bool validLogin = authProvider.SignIn(loginViewModel.Email, loginViewModel.Password);
                if (validLogin)
                {
                    // Redirect the user where you want them to go after successful login
                    return RedirectToAction("UserPlotInfo", "Account");
                  
                } else
                {
                    ModelState.AddModelError(String.Empty, "The username or password are invalid");
                }
            }

            return View(loginViewModel);
        }

        [HttpGet]
        public IActionResult UserPlotInfo()
        {
            AccountInfoVM vm = new AccountInfoVM();

            vm.UserPlots = plotDAO.GetUsersPlots((int)HttpContext.Session.GetInt32("Auth_User_Id"));

            return View(vm);
        }

        [HttpPost]
        public IActionResult UserPlotInfo(AccountInfoVM vm)
        {
            int userId = HttpContext.Session.GetInt32(SessionAuthProvider.UserId).Value;
            if (ModelState.IsValid)
            {
                plotDAO.CreatePlot(userId, vm.plot);
                return RedirectToAction("ViewPlot", vm.plot);
            }
            return View(vm.plot);
            
        }

        [HttpGet]
        public IActionResult LogOff()
        {
            // Clear user from session
            authProvider.LogOff();

            // Redirect the user where you want them to go after logoff
            return RedirectToAction("Login", "Account");
        }
        
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(RegisterViewModel registerViewModel)
        {            
            if (ModelState.IsValid)
            {
                // Register them as a new user (and set default role)
                // When a user registeres they need to be given a role. If you don't need anything special
                // just give them "User".
                authProvider.Register(registerViewModel.Email, registerViewModel.Password, role: "User"); 

                // Redirect the user where you want them to go after registering
                return RedirectToAction("UserPlotInfo", "Account");
            }

            return View(registerViewModel);
        }
    }
}